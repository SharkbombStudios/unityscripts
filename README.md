# README #

This readme contains various scripts created by Sharkbomb Studios for use with the Unity game engine. 

These scripts were created during development of our own games but are publically available here for free use through other developers.

## Links

- Sharkbomb Studios: www.sharkbombs.com
- Unity Engine: www.unity3d.com